FROM node:9.2.1

# Create app directory
WORKDIR /usr/src/app

# Install app dependencies
COPY package.json .

RUN npm install
RUN chown -R node:node ./node_modules

# Bundle app source
COPY . .

EXPOSE 3000
